---
title: About me
---

<center>
<img src="me.png" alt="drawing" style="width:250px;"/>
</center>



I'm a quantum physicist based in Delft, the Netherlands.
I grew up in Loja, Ecuador.

# Education

I obtained my bachelor degree in Physics in 2019 (cum laude) at [Yachay Tech university](https://www.yachaytech.edu.ec).

After that I moved to [TU Delft](https://www.tudelft.nl), where I completed a master (cum laude) in Applied Physics in 2022 with short research stays at [Carlo Beenakker's](https://www.lorentz.leidenuniv.nl/beenakker/) group and [Orange Quantum Systems](https://orangeqs.com).

Currently, I'm doing my PhD at the [Quantum Tinkerer](https://quantumtinkerer.tudelft.nl) group.

# Research

I specialize in theory of condensed matter systems and quantum computation.
My research interests include:

- superconducting excitations such as Majorana or Andreev states,
- spin qubits in semiconductors (Si, Ge),
- transport signatures of topological excitations.

# Interests

I enjoy learning about software and automation. I have contributed to packages such as:

- [Adaptive](http://adaptive.readthedocs.io)

Other personal interests include rock climbing and bouldering, learning new languages, reading books, travelling around, and playing the guitar and recording music.

## Contact information

- email: [jd.torres1595@gmail.com](mailto:jd.torres1595@gmail.com)
- address: [E216, Lorentzweg 1, 2628CJ, Delft, The Netherlands](https://www.google.com/maps/place/Lorentzweg+1,+2628+CJ+Delft,+Netherlands/data=!4m2!3m1!1s0x47c5b58d85078c8b:0xc12624b4b334b89?sa=X&ved=2ahUKEwjg3rWSkZ78AhU_ZzABHYC8AXoQ8gF6BAgSEAE).
- orcid: [0000-0001-8455-2993](https://orcid.org/0000-0001-8455-2993)
- github: [@juandaanieel](https://github.com/juandaanieel)