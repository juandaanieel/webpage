# ---
# jupyter:
#   jupytext:
#     cell_metadata_filter: -all
#     custom_cell_magics: kql
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.11.2
#   kernelspec:
#     display_name: Python kwant
#     language: python
#     name: python3
# ---

# %%
from pathlib import Path
import os
import arxiv
import requests
from youtube_search import YoutubeSearch


# %%


records_api_url = 'https://zenodo.org/api/records'
client = arxiv.Client()

search = arxiv.Search(
  query = "au:juan daniel torres luna",
  max_results = 100,
)

results = client.results(search)

my_papers = []
my_names = ['Juan Daniel Torres Luna', 'Juan Daniel Torres']
zenodo_dois = []
youtube_urls = []
for r in results:
    for author in r.authors:
        if author.name in my_names:
            my_papers.append(r)
            # look for Zenodo paper
            title = str('title:"'+r.title+'"')
            res = requests.get(
                records_api_url,
                params={
                    'q': title,
                    'size': 1,
                    'page': 1
                    }
                )
            try:
                zenodo_dois.append(res.json()['hits']['hits'][0]['doi'])
            except:
                zenodo_dois.append(None)
            
            # Look for online presentation in VSF
            title_youtube = '"'+r.title+'"'
            results = YoutubeSearch(title_youtube, max_results=1).to_dict()
            if results[0]['channel'] == 'Virtual Science Forum':
                youtube_urls.append(results[0]['url_suffix'])
            else:
                youtube_urls.append(None)


text = f"""

# How I publish my research \n

I believe that science should be open and accessible to everyone.
Therefore, I try to make all my publications open access following [FAIR](https://www.go-fair.org/fair-principles/) principles.
I use:

- [arXiv](https://arxiv.org/) to publish preprints of my papers.
- [SciPost](https://scipost.org/) to publish (mostly) all my papers because they have a public peer-review process.
- [Zenodo](https://zenodo.org/) to publish all my code and data.
- [Virtual Science Forum](https://www.youtube.com/channel/UCzv4b1t6y4bQ7O0z3pR1z8w) to present my research via online talks.

\n\n
"""

text += f"""
# List of publications \n
"""

for i, paper in enumerate(my_papers):
    text += f""" - {paper.title}\n\n\tAuthors: """
    for author in paper.authors:
        if author.name in my_names:
            text += f"""**{author.name}**, """
        else:
            text += f"""{author.name}, """
    text = text[:-2]
    text += f"""\n\n\tJournal: """
    link = str(paper.links[0])
    if paper.journal_ref is None:
        text += f"""[{str(paper.entry_id)}]({link})\n\n"""
    else:
        text += f"""[{paper.journal_ref}]({link})\n\n"""
    if zenodo_dois[i] is not None:
        text += f"""\tExtra information: """
        text += f"""[Code and data](https://doi.org/{zenodo_dois[i]}), """
    if youtube_urls[i] is not None:
        text += f"""[Online presentation](https://www.youtube.com{youtube_urls[i]}), """
    text = text[:-2]
    text += """\n\n"""



path = Path(os.getcwd()) / 'docs' / 'papers.md'
with open(path, 'a') as file:
    file.write(text)


# %%
